### Instructions

Firstly, run ServerSide.java either directly from IDE or buy running `java ServerSide` (first compile it with javac Serverside.java)
Then run ClientSide.java in the same way.

## Commands

# SET
Stores bytes to a key.

```
Example:
Client: set\tfoo\t3\nbar
Server: ok\t0\n
```

# GET
Retrieves the bytes for a key.

```
Example:
Client: get\tfoo\t0\n
Server: ok\t3\nbar
Example:
Client: get\tnon-existing-key\t0\n
Server: key not found\t0\n
```

# DELETE
Delete a key.

```
Example:
Client: delete\tfoo\t0\n
Server: ok\t0\n
```

# STATS
Retrieve specific server stats:

* num_keys (number of keys stored)
* db_size (size in bytes of database file on disk)
* num_connections (number of active TCP connections to the server)

```
Example:
Client: stats\tnum_keys\t0\n Server: ok\t4\n1337
```