import static command.Constants.CONNECTION_KEY;
import static java.lang.String.valueOf;
import static java.lang.System.err;
import static java.lang.System.exit;

import command.CommandFactory;
import command.SetCommand;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerSide {

  public static void main(String[] args) {

    final int max_number_of_connections = 5;
    ServerSocket serverSocket = null;
    AtomicInteger connectionCounter = new AtomicInteger();

    try {
      serverSocket = new ServerSocket(3);
    } catch (IOException e) {
      err.println("Error:" + e.getMessage());
      exit(1);
    }

    for (int i = 0; i < max_number_of_connections; i++) {
      ServerSocket finalServerSocket = serverSocket;
      Runnable runnable =
          () -> {
            try {
              final Socket clientSocket = finalServerSocket.accept();
              new SetCommand()
                  .processMessages(CONNECTION_KEY, valueOf(connectionCounter.incrementAndGet()));
              PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
              BufferedReader in =
                  new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
              String firstInputLine;
              String secondInputLine;

              while ((firstInputLine = in.readLine()) != null
                  && (secondInputLine = in.readLine()) != null) {
                CommandFactory commandFactory = new CommandFactory();
                final String outputLine =
                    commandFactory.processMessage(firstInputLine, secondInputLine);
                out.println(outputLine);
                if ("Exit".equalsIgnoreCase(outputLine)) break;
              }
              out.close();
              in.close();
              clientSocket.close();
              finalServerSocket.close();
              new SetCommand()
                  .processMessages(CONNECTION_KEY, valueOf(connectionCounter.decrementAndGet()));

            } catch (IOException e) {
              err.println("Error:" + e.getMessage());
              exit(1);
            }
          };
      Executors.newCachedThreadPool().execute(runnable);
    }
  }
}
