package command;

import static command.Utils.constructRespone;

public class CommandFactory {

  public CommandFactory() {}

  public String processMessage(String firstLine, String payloadBytes) {

    final String[] split = firstLine.split("\t");
    if (split.length != 3) {
      constructRespone("Doesn't work like this", "0", null);
    }

    final String commandName = split[0];
    final String keyName = split[1];
    final String payloadSize = split[2];

    String response;

    switch (commandName.toUpperCase()) {
      case "SET":
        final String validate = validate(keyName, Integer.parseInt(payloadSize), payloadBytes);
        response =
            validate == null ? new SetCommand().processMessages(keyName, payloadBytes) : validate;
        break;
      case "GET":
        response = new GetCommand().processMessages(keyName, null);
        break;
      case "DELETE":
        response = new DeleteCommand().processMessages(keyName, null);
        break;
      case "STATS":
        response = new StatsCommand().processMessages(keyName, null);
        break;
      default:
        return constructRespone("Invalid command", "0", null);
    }

    return response;
  }

  private String validate(String key, int valueSize, String value) {
    String response = null;
    if (key.length() > 100) {
      response = "Key size cannot exceed 100";
    } else if (valueSize > 1000000) {
      response = "Payload size cannot exceed 1000000";
      return response;
    } else if (value.length() != valueSize) {
      response = "Given value size and the size of actual value does not match";
    }
    return response;
  }
}
