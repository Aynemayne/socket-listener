package command;

public interface BaseCommand {
  String processMessages(String firstLine, String secondLine);
}
