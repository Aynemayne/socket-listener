package command;

import static command.Constants.FILE_PATH;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class DeleteCommand implements BaseCommand {
  @Override
  public String processMessages(String key, String value) {
    String response;

    try {
      OutputStream output = new FileOutputStream(FILE_PATH);
      Properties properties = new Properties();
      properties.remove(key);
      properties.store(output, null);
      response = "OK";
    } catch (IOException e) {
      response = e.getMessage();
    }

    return Utils.constructRespone(response, "0", null);
  }
}
