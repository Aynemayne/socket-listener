package command;

import static command.Constants.FILE_PATH;
import static command.Constants.MAX_FILE_SIZE;
import static command.Utils.constructRespone;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class SetCommand implements BaseCommand {

  @Override
  public String processMessages(String key, String value) {

    String response;

    try {
      File file = new File(FILE_PATH);
      if (file.length() > MAX_FILE_SIZE) {
        return constructRespone("Cannot write to file, file size exceeded", "0", null);
      } else if (file.length() + value.length() > MAX_FILE_SIZE) {
        return constructRespone("Cannot write to file, value is too big", "0", null);
      }

      Properties properties = new Properties();
      properties.load(new FileReader(FILE_PATH));
      properties.put(key, value);
      response = "OK";
    } catch (IOException e) {
      response = e.getMessage();
    }

    return constructRespone(response, "0", null);
  }
}
