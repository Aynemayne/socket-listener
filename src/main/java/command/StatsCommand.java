package command;

import static command.Constants.FILE_PATH;
import static command.Utils.constructRespone;
import static java.lang.String.valueOf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class StatsCommand implements BaseCommand {
  @Override
  public String processMessages(String requestType, String secondLine) {
    String response;

    switch (requestType.toUpperCase()) {
      case "NUM_KEYS":
        response = getNumberOfKeyFromDatabase();
        break;
      case "DB_SIZE":
        response = getDBSize();
        break;
      case "NUM_CONNECTIONS":
        response = getNumberOfConnections();
        break;
      default:
        response =
            constructRespone(
                "Allowed requests are num_key, db_size and num_connections", "0", null);
        break;
    }

    return response;
  }

  private String getNumberOfKeyFromDatabase() {
    try {
      Properties properties = new Properties();
      properties.load(new FileInputStream(FILE_PATH));
      final String size = valueOf(properties.keySet().size());
      return constructRespone("OK", valueOf(size.length()), size);
    } catch (IOException e) {
      return constructRespone(e.getMessage(), "0", null);
    }
  }

  private String getDBSize() {
    File file = new File(FILE_PATH);
    if (!file.exists()) {
      return constructRespone("file cannot found in path:" + FILE_PATH, "0", null);
    }

    final String fileSize = valueOf(file.length());
    return constructRespone("OK", valueOf(fileSize.length()), fileSize);
  }

  private String getNumberOfConnections() {
    try {
      Properties properties = new Properties();
      properties.load(new FileInputStream(FILE_PATH));
      final String connections = valueOf(properties.get(Constants.CONNECTION_KEY));
      return constructRespone("OK", valueOf(connections.length()), connections);
    } catch (IOException e) {
      return constructRespone(e.getMessage(), "0", null);
    }
  }
}
