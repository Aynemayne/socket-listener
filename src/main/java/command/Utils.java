package command;

public class Utils {

  public static String constructRespone(String message, String payloadSize, String payload) {
    return String.format("%s\t%s\n%s", message, payloadSize, payload);
  }
}
