package command;

public final class Constants {
  public static final String FILE_PATH = "src/main/resources/config.properties";
  public static final long MAX_FILE_SIZE = 25000000L;
  public static final String CONNECTION_KEY = "number_of_connections";

  private Constants() {}
}
