package command;

import static command.Constants.FILE_PATH;
import static command.Utils.constructRespone;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class GetCommand implements BaseCommand {
  @Override
  public String processMessages(String key, String value) {

    try {
      Properties prop = new Properties();
      prop.load(new FileInputStream(FILE_PATH));

      final String propertyValue = prop.getProperty(key);
      return propertyValue != null
          ? constructRespone("OK", String.valueOf(propertyValue.length()), propertyValue)
          : constructRespone("key not found", "0", null);
    } catch (IOException e) {
      return constructRespone(e.getMessage(), "0", null);
    }
  }
}
