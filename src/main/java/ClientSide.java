import static java.lang.System.exit;
import static java.lang.System.in;
import static java.lang.System.out;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientSide {

  public static void main(String[] args) throws IOException {

    Socket echoSocket = null;
    PrintWriter printWriter = null;
    BufferedReader bufferedReader = null;
    BufferedReader stdIn = null;
    String firstLine;
    String secondLine;

    try {
      echoSocket = new Socket("localhost", 3);
      bufferedReader = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
      stdIn = new BufferedReader(new InputStreamReader(in));
      while ((firstLine = stdIn.readLine()) != null && (secondLine = stdIn.readLine()) != null) {
        // Sending it like this to process the payload bytes line in one go
        printWriter = new PrintWriter(echoSocket.getOutputStream(), true);
        printWriter.println(firstLine + "\n" + secondLine);
        out.println("Response: " + bufferedReader.readLine());
      }
    } catch (IOException e) {
      out.println("Failure: " + e.getMessage());
      exit(1);
    }

    printWriter.close();
    bufferedReader.close();
    stdIn.close();
    echoSocket.close();
  }
}
